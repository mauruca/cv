FROM nginx:alpine

MAINTAINER Mauricio Pinheiro <mauricio@ur2.com.br>

# Copy web site
COPY . /usr/share/nginx/html
RUN rm /usr/share/nginx/html/Dockerfile
